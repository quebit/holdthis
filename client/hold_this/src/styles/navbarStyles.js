const activeNavStyles = {
  color: '#9F9F9F',
  backgroundColor: '#222222',
}

const buttonContentStyles = {
	margin: 'auto'
}

export default {activeNavStyles, buttonContentStyles}