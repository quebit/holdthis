const PasteBin = require('./models')
const ms = require('ms');
const creds = require('./emailconfig');
var mailgun = require('mailgun-js')({apiKey: creds.APIKEY, domain: creds.DOMAIN});

/**
 * @swagger
 * /createPastebin:
 *    post:
 *      tags:
 *          - Pastebin
 *      summary: Create a new pastebin.
 *      description:
 *      consumes:
 *        - application/json
 *      produces:
 *         - application/json
 *      parameters:
 *        - name: body
 *          in: body
 *          schema:
 *            type: object
 *            properties:
 *              text:
 *                type: string
 *                required: true
 *                description: Pastebin's text
 *              isProtected:
 *                type: "boolean"
 *                required: true
 *                description: If this pastebin has a password
 *              password:
 *                type: string
 *                required: false
 *                description: Pastebin's password
 *              expirationTime:
 *                type: string
 *                required: false
 *                default: 1d
 *                description: Number of milliseconds from current time in which the pastebin expires accepts '1m','15m', '1h', '3h','6h','12h', '1d' all other values will result in the pastebin getting a expirationTime of 1 day after time of request
 *              
 *      responses:
 *        201:
 *          description: Receive pastebin's text content and ID
 */

exports.initPastebin =  async function(req, res) {
	let pastebin = new PasteBin()
	pastebin = await pastebin.createPasteBin(req);
  res.status(201);
  res.json({'text': pastebin.text, 'slug': pastebin.slug});
}


/**
 * @swagger
 * /bin/{slug}:
 *    get:
 *      tags: 
 *          - Pastebin
 *      summary: Find pastebin by ID. 
 *      description: Returns a pastebin's text content or asks for password
 *      produces:
 *         - application/json
 *      parameters:
 *         - name: "slug"
 *           in: "path"
 *           description: "ID of pastebin to return"
 *           required: true
 *           type: string
 *          
 *      responses:
 *        200:
 *          description: Receive pastebin's text.
 *        403:
 *          description: Pastebin requires a password but none was supplied.
 *        404:
 *          description: Pastebin never existed in the first place or it expired.
 *
 */
exports.getPastebinOr404 = function(req, res) { 
  PasteBin.findOne({'slug':req.params.slug}, function(err, pastebin) {
    if(pastebin !== null && pastebin.notExpired()) {
      if(pastebin.isProtected) {
        res.status(403);
        res.json({'response': 'This pastebin requires a password but none was supplied'}) 
      }
      else  {
        res.status(200);
        res.json({'text': pastebin.text});
      }
    }
    else {
      res.status(404);
      res.json({'text': 'This pastebin never existed in the first place or it expired.'});
    }
  }) 
}

/**
 * @swagger
 * /bin/{slug}:
 *    post:
 *      tags: 
 *          - Pastebin
 *      summary: Find password protected pastebin's text by ID. 
 *      description: 
 *          Returns a pastebin's text content or alerts cosnumer the password is wrong.
 *      produces:
 *        - application/json
 *      consumes:
 *        - application/json
 *      parameters:
 *        - name: "slug"
 *          in: "path"
 *          description: "ID of pastebin to return"
 *        - name: body
 *          in: body
 *          schema:
 *            type: object
 *            properties:
 *              password:
 *                type: string
 *                required: true
 *                description: Pastebin's password
 *      responses:
 *        200:
 *          description: Receive pastebin's text.
 *        403:
 *          description: Password incorrect or no password was supplied. 
 *        404:
 *          description: Pastebin never existed in the first place or it expired.
 */
exports.getProtectedPastebinOr404 = function(req, res) { 
  PasteBin.findOne({'slug':req.params.slug}, function(err, pastebin) {
    if(pastebin !== null && pastebin.notExpired()) {
      if(req.body.password !== undefined) { 
        pastebin.comparePassword(req.body.password, function(err, isMatch) { 
          if(!isMatch){
            res.status(403);
            res.json({'response': 'Password incorrect'})
          }
          else {
            res.status(200);
            res.json({'text': pastebin.text});
          }
        }); 
      }
      else {
        res.status(403);
        res.json({'response': 'This pastebin requires a password but none was supplied'}) 
      }
    }
    else {
        res.status(404);
        res.json({'text': 'This pastebin never existed in the first place or it expired.'});
    } 
  }) 
}

exports.sendContactMail = async function(req, res) {
  const name = req.body.name
  const email = req.body.email
  const subject = req.body.subject
  const message = req.body.message
  const content = `name: ${name} \n email: ${email} \n subject: ${subject} message: \n ${message} `
  const mail = {
    from: 'que.holdthis@gmail.com', // In case user enters an invalid email, mailgun vaildation costs money
    to: 'que.holdthis@gmail.com',  
    subject: `CONTACT: ${subject}`,  
    text: content
  }
  mailgun.messages().send(mail, (err, data) => {
    if (err) {
      res.status(502);
      res.json({response: 'Failed to send message'})
    } 
    else {
      res.status(200);
      res.json({response: 'Message sent!'})
    }
  })
}
