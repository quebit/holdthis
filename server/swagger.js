const swaggerJsdoc = require('swagger-jsdoc');
const options = {
	// List of files to be processed.
  apis: ['./controllers.js' ],
  // You can also set globs for your apis
  // e.g. './routes/*.js'
  basePath: '/',
  swaggerDefinition: {
  	info: {      
   		description: 'Public API doc for holdthis.xyz',      
   		swagger: '2.0',      
   		title: 'holdthis API',      
   		version: '0.0.1',
   	}
  }
}

const specs = swaggerJsdoc(options);
module.exports = specs