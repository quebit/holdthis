const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt')
const SALT_WORK_FACTOR = 10;
const ms = require('ms');
const expirationTimes = ['1m','15m', '1h', '3h','6h','12h', '1d'] //it will default to 24h on its on
const helperFunctions = require('./helperFunctions')


let PasteBin = new Schema({
	slug: {
		type: String
	},
	text: {
		type: String
	},
  expirationDate: {
  	type: Date
  },
  isProtected: {
  	type: Boolean, default: false
  },
  password: { 
  	type: String, required: false 
  }  
})

PasteBin.pre('save', function(next) {
  var pastebin = this;
  // only hash the password if it has been modified (or is new) 
  if (!pastebin.isModified('password') || !pastebin.isProtected ) { return next(); }
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) { return next(err); }
    bcrypt.hash(pastebin.password, salt, function(err, hash) {
      if (err) {return next(err)};
      pastebin.password = hash;
      next();
    });
  });
});

PasteBin.methods.notExpired = function() {
  if(this.expirationDate > Date.now()) {
    return true
  }
  else {
    return false
  }
}

PasteBin.methods.comparePassword = function(candidatePassword, callBack) { 
	bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
		if (err) return callBack(err);
		callBack(null, isMatch)
	});	
} 

PasteBin.methods.createPasteBin =  function(req) {
  var pastebin = this; 
  pastebin.slug = helperFunctions.randomSlug(9) // TODO: change this to helperFunctions.randomSlug()
  pastebin.text = req.body.text
  pastebin.isProtected = req.body.isProtected
  if(req.body.isProtected) {
    pastebin.password = req.body.password    
  }
  if(expirationTimes.includes(req.body.expirationTime)) {
    pastebin.expirationDate = Date.now() + ms(req.body.expirationTime);
  }
  else { //in case any nefarious force wants a forever PasteBin
    pastebin.expirationDate = Date.now() + ms('1d');
  }
  pastebin.save()
  return {'text': req.body.text, 'slug': pastebin.slug }
}

module.exports = mongoose.model('PasteBin', PasteBin);
