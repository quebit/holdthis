const express = require('express');
const routes = express.Router('./models');
const PasteBin = require('./models')
let controllers = require('./controllers')
const swaggerUi = require('swagger-ui-express');
const specs = require('./swagger');

routes.route('/bin/:slug').get(controllers.getPastebinOr404);
routes.route('/bin/:slug').post(controllers.getProtectedPastebinOr404);
routes.route('/createPastebin').post(controllers.initPastebin)  
routes.route('/sendContactMail').post(controllers.sendContactMail) 
routes.use('/', swaggerUi.serve);
routes.get('/', swaggerUi.setup(specs));

module.exports = routes